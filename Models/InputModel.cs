﻿using System.Collections.Generic;

namespace Practice_Round___Hash_Code_2022.Models
{
    public class InputModel
    {

        public IList<ClientsModel> Clients { get; set; } = new List<ClientsModel>();
    }
}