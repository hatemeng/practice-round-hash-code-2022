﻿using System.Collections.Generic;

namespace Practice_Round___Hash_Code_2022.Models
{
    public class ClientsModel
    {
        public List<string> LikedIngredients { get; set; }
        public List<string> DislikedIngredients { get; set; }
    }
}