﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Practice_Round___Hash_Code_2022
{
    internal class Program
    {
        public static long totalResult = 0;
        public static void Main(string[] args)
        {
            foreach (var type in Enum.GetNames(typeof(Inputs)))
            {
                new Solver(type).Run();
            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Total Result : {totalResult}");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("Press any key to exit");
            Console.ReadLine();
        }
    }
}