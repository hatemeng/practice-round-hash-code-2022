﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Practice_Round___Hash_Code_2022.Models;

namespace Practice_Round___Hash_Code_2022
{
    public enum Inputs { a_an_example, b_basic, c_coarse, d_difficult, e_elaborate }

    public class Solver
    {
        private readonly string _inputFileName;

        public Solver(string inputFile)
        {
            _inputFileName = inputFile;
        }

        private string BasePath()
        {
            var path = Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory);
            return path.Substring(0, path.LastIndexOf("bin"));
        }

        private (InputModel, List<string>) ParseInputFile()
        {
            // organize
            var result = new InputModel();
            var ingredients = new List<string>();
            var fileData = File.ReadAllText($"{BasePath()}Inputs\\{_inputFileName}.in.txt");
            var lines = fileData.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);
            var numberOfClients = int.Parse(lines.First());
            lines = lines.Skip(1).ToArray();
            // act

            // loop over clients and skip the first line
            for (var i = 0; i < numberOfClients; i++)
            {
                // liked or disliked
                var itemIndex = i * 2;
                var userLikedIngredients = lines[itemIndex].Split(new[] { " " },
                    StringSplitOptions.RemoveEmptyEntries).Skip(1).ToList();
                var userDisLikedIngredients = lines[itemIndex + 1].Split(new[] { " " },
                    StringSplitOptions.RemoveEmptyEntries).Skip(1).ToList();

                // add all ingredients
                ingredients.AddRange(userLikedIngredients.Union(userDisLikedIngredients).Except(ingredients));

                result.Clients.Add(new ClientsModel
                {
                    LikedIngredients = userLikedIngredients,
                    DislikedIngredients = userDisLikedIngredients
                });
            }


            return (result, ingredients.Distinct().ToList());
        }

        private void ToFile(string output)
        {
            File.WriteAllText($"{BasePath()}Outputs\\{_inputFileName}.out.txt", output);
        }

        public void Run()
        {
            // organize
            var result = new List<string>();

            var (inputData, allIngredients) = ParseInputFile();
            // act
            allIngredients.ForEach(ingredient =>
            {
                var usersLiked = inputData.Clients.Count(client => client.LikedIngredients.Contains(ingredient));
                var usersDisLiked = inputData.Clients.Count(client => client.DislikedIngredients.Contains(ingredient));

                if (usersLiked > usersDisLiked)
                    result.Add(ingredient);

            });

            var numberOfClients = inputData.Clients.Count(client =>
                client.LikedIngredients.All(item => result.Contains(item)) &&
                client.DislikedIngredients.All(item => !result.Contains(item)));
            Program.totalResult += numberOfClients;
            // output
            ToFile($"{result.Count} {string.Join(" ", result)}");
            Console.WriteLine($"Number of clients of data {_inputFileName} are : {numberOfClients}");

        }
    }
}